#installation
```shell
$ yarn install
$ react-native link
$ react-native run-ios
```
---
> Example : (Zwaar Example)
```shell
- App has the following Routes/Components :-
1- Login
2- Register
3- Profile
4- Home (supposed to be List of Student's Posts)
```
-----
> General Notes :-
- all api calls are just fake data.
- i did not use the api u provided.
- forms data are validated.
- i did not give much concern for Layout Design.
---
```shell
- in real apps i do not call any api request from component. 
i fire an action then use saga to 
fire async call to geneator action that will handle the api request 
then store all data in my state using redux.
for example : 
```
```javascript
export function* startAppBootStrap() {
  try {
    let device_id = DeviceInfo.getUniqueID();
    yield all([
      call(enableLoading),
      call(defaultLang),
      put({type: actions.GET_DEVICE_ID, payload: device_id})
    ]);
    const api_token = yield call(helpers.getAuthToken);
    const registerRequest = yield call(api.getRegisterRequest, device_id);
    const galleries = yield call(api.getGalleries, {
      type: 'user',
      element_id: 1
    });
    if (!validate.isEmpty(galleries)) {
      yield put({type: actions.SET_GALLERIES, payload: galleries});
    }
    if (!validate.isEmpty(registerRequest)) {
      // add the registerRequest to the state according to the device id;
      yield put({type: actions.GET_REGISTER_REQUEST, payload: registerRequest});
    }
    if (!validate.isEmpty(api_token)) {
      const user = yield call(api.authenticated, api_token);
      if (
        !validate.isEmpty(user) &&
        validate.isObject(user) &&
        !validate.isEmpty(api_token)
      ) {
        yield all([
          call(startLoginScenario, user),
          call(toggleGuest, false),
          put(
            NavigationActions.navigate({
              routeName: 'Home'
            })
          )
        ]);
      } else {
        yield call(toggleGuest, true);
      }
    }
    const sliders = yield call(api.getHomeSliders, 'is_splash');
    const settings = yield call(api.getSettings);
    const roles = yield call(api.getRoles);
    if (!validate.isEmpty(settings) && !validate.isEmpty(roles)) {
      yield all([
        put({type: actions.GET_ROLES, payload: roles}),
        put({type: actions.GET_SETTINGS, payload: settings})
      ]);
    } else {
      throw new Error('settings error or roles .. system error');
    }
    if (!validate.isEmpty(sliders)) {
      yield all([
        put({type: actions.SET_HOME_SLIDERS, payload: sliders}),
        call(toggleBootStrapped, true),
        call(disableLoading)
      ]);
    } else {
      yield all([
        call(toggleBootStrapped, true),
        call(disableLoading),
        put(
          NavigationActions.navigate({
            routeName: 'Home'
          })
        )
      ]);
    }
  } catch (e) {
    yield all([call(disableLoading), call(enableErrorMessage, e.message)]);
  }
}
```
