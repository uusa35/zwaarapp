import React, {Component} from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import {RkTextInput, RkButton} from 'react-native-ui-kitten';
import validate from 'validate.js';
import {RNToasty} from 'react-native-toasty';
import {loginConstrains} from './../constrains';
import {images, width, axiosInstance} from './../constants';

export default class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {email: 'ahmed@asadgroup.com', password: 'asdasd'};
  }

  _doSubmitLogin = () => {
    const {email, password} = this.state;
    const {navigation} = this.props;
    const result = validate({email, password}, loginConstrains);
    if (!validate.isEmpty(result)) {
      return RNToasty.Warn({
        title: JSON.stringify(result[Object.keys(result)[0]]),
        titleSize: 25
      });
    } else {
      return axiosInstance
        .post('authenticate', {email, password})
        .then(r => navigation.navigate('Home', {projects: r.data}).bind(this))
        .catch(e =>
          RNToasty.Error({
            title: e.response.data.message,
            titleSize: 25
          })
        );
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <Text style={styles.welcome}>Student Login</Text>
          <Image source={images.logo} style={{width: 100, height: 100}} />
          <RkTextInput
            rkType="frame"
            label="Email"
            labelStyle={{color: 'grey', width: 70}}
            inputStyle={{
              backgroundColor: 'lightgray',
              height: 50
            }}
            style={{height: 100}}
            onChangeText={e => this.setState({email: e})}
          />
          <RkTextInput
            rkType="rounded"
            label="Password"
            labelStyle={{color: 'grey', width: 70}}
            inputStyle={{
              backgroundColor: 'lightgray',
              height: 50
            }}
            style={{height: 100}}
            onChangeText={e => this.setState({password: e})}
          />
          <View
            style={{
              width: width - 100,
              justifyContent: 'center',
              alignItems: 'center'
            }}>
            <RkButton
              onPress={() => this._doSubmitLogin()}
              style={{backgroundColor: 'red', width: width - 30}}>
              Submit
            </RkButton>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black'
  },
  wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
    padding: 15
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 15,
    color: 'white'
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5
  }
});
