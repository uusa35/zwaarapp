import React, {Component} from 'react';
import {View, Text, FlatList, Image, StyleSheet} from 'react-native';
import {RkCard} from 'react-native-ui-kitten';
import {axiosInstance, images, width} from '../constants';

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {posts: []};
  }

  componentWillMount() {
    axiosInstance('https://jsonplaceholder.typicode.com/posts')
      .then(r => this.setState({posts: r.data}))
      .catch(e => console.log(e));
  }

  _renderItem = item => {
    return (
      <RkCard>
        <View rkCardHeader>
          <Text style={styles.subTitle}>{item.title}</Text>
        </View>
        <Image rkCardImg source={images.logo} />
        <View rkCardContent>
          <Text> {item.body}</Text>
        </View>
      </RkCard>
    );
  };

  render() {
    const {posts} = this.state;
    return (
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'white'
        }}>
        <Text style={styles.title}>List of Student's Posts</Text>
        <FlatList
          data={posts}
          renderItem={({item}) => this._renderItem(item)}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 30,
    color: 'black',
    margin: 15,
    borderWidth: 1,
    borderColor: 'lightgrey',
    width: width,
    padding: 20,
    textAlign: 'center'
  },
  subTitle: {
    textAlign: 'center',
    color: 'grey'
  }
});
