import React, {Component} from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import {RkCard} from 'react-native-ui-kitten';
import {images, width} from '../constants';

export default class ProfileScreen extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {navigation} = this.props;
    const {user} = navigation.state.params;
    return (
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'white'
        }}>
        <RkCard>
          <View rkCardHeader>
            <Text style={styles.subTitle}>{user.name}</Text>
          </View>
          <Image rkCardImg source={images.logo} />
          <View rkCardContent>
            <Text> {user.email}</Text>
            <Text> {user.description}</Text>
          </View>
        </RkCard>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 30,
    color: 'black',
    margin: 15,
    borderWidth: 1,
    borderColor: 'lightgrey',
    width: width,
    padding: 20,
    textAlign: 'center'
  },
  subTitle: {
    textAlign: 'center',
    color: 'grey'
  }
});
