import React, {Component} from 'react';
import {StyleSheet, Text, View, Image, ScrollView} from 'react-native';
import {RkTextInput, RkButton} from 'react-native-ui-kitten';
import validate from 'validate.js';
import {RNToasty} from 'react-native-toasty';
import {registerConstrains} from './../constrains';
import {images, width, axiosInstance} from './../constants';

export default class RegisterStudent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      password: '',
      mobile: '',
      description: ''
    };
  }

  _doRegister = () => {
    const {name, email, password, mobile, description} = this.state;
    const {navigation} = this.props;
    const result = validate(
      {email, password, mobile, description},
      registerConstrains
    );
    if (!validate.isEmpty(result)) {
      return RNToasty.Warn({
        title: JSON.stringify(result[Object.keys(result)[0]]),
        titleSize: 25
      });
    } else {
      return axiosInstance
        .post(`request`, {name, email, mobile, description, type: 'student'})
        .then(r => navigation.navigate('Profile', {user: r.data}).bind(this))
        .catch(e =>
          RNToasty.Error({
            title: e.response.data.message,
            titleSize: 25
          })
        );
    }
  };

  render() {
    return (
      <ScrollView style={{backgroundColor: 'black'}}>
        <View style={styles.wrapper}>
          <Text style={styles.welcome}>Student Register</Text>
          <Image source={images.logo} style={{width: 100, height: 100}} />
          <RkTextInput
            rkType="frame"
            label="Name"
            labelStyle={styles.labelStyle}
            inputStyle={styles.inputStyle}
            style={{height: 100}}
            onChangeText={e => this.setState({name: e})}
          />
          <RkTextInput
            rkType="frame"
            label="Email"
            labelStyle={styles.labelStyle}
            inputStyle={styles.inputStyle}
            style={{height: 100}}
            onChangeText={e => this.setState({email: e})}
          />
          <RkTextInput
            rkType="rounded"
            label="Password"
            labelStyle={styles.labelStyle}
            inputStyle={styles.inputStyle}
            style={{height: 100}}
            onChangeText={e => this.setState({password: e})}
          />
          <RkTextInput
            rkType="rounded"
            label="Mobile"
            labelStyle={styles.labelStyle}
            inputStyle={styles.inputStyle}
            style={{height: 100}}
            onChangeText={e => this.setState({mobile: e})}
          />
          <RkTextInput
            rkType="rounded"
            label="Description"
            labelStyle={styles.labelStyle}
            inputStyle={styles.inputStyle}
            style={{height: 100}}
            onChangeText={e => this.setState({description: e})}
          />
          <View style={styles.btnContainer}>
            <RkButton onPress={() => this._doRegister()} style={styles.btn}>
              Submit
            </RkButton>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black'
  },
  wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
    padding: 15
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 15,
    color: 'white'
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5
  },
  btnContainer: {
    width: width - 100,
    justifyContent: 'center',
    alignItems: 'center'
  },
  btn: {
    backgroundColor: 'red',
    width: width - 30
  },
  inputStyle: {
    backgroundColor: 'lightgray',
    height: 50
  },
  labelStyle: {
    color: 'grey',
    width: 70
  }
});
