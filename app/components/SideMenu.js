import React, {Component} from 'react';
import {View, FlatList, Image, StyleSheet} from 'react-native';
import {List, ListItem} from 'react-native-elements';
import {routesList, images} from './../constants';
import PropTypes from 'prop-types';

export default class SideMenu extends Component {
  constructor(props) {
    super(props);
  }
  _renderRow = item => {
    const {navigation} = this.props;
    console.log('item', item.routeName);
    return (
      <ListItem
        key={item.name}
        title={item.slugName}
        leftIcon={{name: item.iconName}}
        onPress={() => navigation.navigate(item.routeName)}
      />
    );
  };

  render() {
    return (
      <View style={{marginTop: '20%', flex: 1, backgroundColor: 'transparent'}}>
        <Image source={images.logo} style={styles.logo} />
        <List>
          <FlatList
            data={routesList}
            renderItem={({item}) => this._renderRow(item)}
            keyExtractor={item => item.name}
          />
        </List>
      </View>
    );
  }
}

SideMenu.propTypes = {
  navigation: PropTypes.object.isRequired
};

const styles = StyleSheet.create({
  logo: {
    width: 100,
    height: 100,
    alignSelf: 'center'
  }
});
