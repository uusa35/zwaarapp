import React from 'react';
import {Dimensions} from 'react-native';
import axios from 'axios';
export const {height, width} = Dimensions.get('window');
export const images = {
  logo: require('./../assets/logo.png')
};
export const apiLink = 'http://app.asadgroup.com/api/';
export const axiosInstance = axios.create({
  baseURL: apiLink,
  headers: {lang: 'en'}
});
export const routesList = [
  {
    routeName: 'Home',
    slugName: 'Home',
    iconName: 'home'
  },
  {
    routeName: 'Login',
    slugName: 'Login',
    iconName: 'assignment-ind'
  },
  {
    routeName: 'Register',
    slugName: 'Register Student',
    iconName: 'assignment'
  }
];
