import React, {Component} from 'react';
import RootNavigator from './AppNavigator';

export default class App extends Component {
  render() {
    return <RootNavigator />;
  }
}
