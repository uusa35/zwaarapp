import React from 'react';
import {
  createStackNavigator,
  createDrawerNavigator
} from 'react-navigation';
import LoginScreen from './screens/LoginScreen';
import HomeScreen from './screens/HomeScreen';
import SideMenu from './components/SideMenu';
import HeaderLeft from './components/HeaderLeft';
import RegisterStudent from './screens/RegisterStudent';
import ProfileScreen from './screens/ProfileScreen';

const AppNavigator = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: ({navigation}) => ({
        headerTitle: 'Home',
        headerLeft: <HeaderLeft navigation={navigation} />
      })
    },
    Login: {
      screen: LoginScreen,
      navigationOptions: ({navigation}) => ({
        headerTitle: 'Login'
      })
    },
    Register: {
      screen: RegisterStudent,
      navigationOptions: ({navigation}) => ({
        headerTitle: 'Register Student'
      })
    },
    Profile: {
      screen: ProfileScreen,
      navigationOptions: ({navigation}) => ({
        headerTitle: 'Student Profile'
      })
    }
  },
  {
    initialRouteName: 'Home',
    order: ['Home', 'Login', 'Register']
  }
);

const RootNavigator = createDrawerNavigator(
  {
    Screens: {
      screen: AppNavigator
    }
  },
  {
    contentComponent: ({navigation}) => <SideMenu navigation={navigation} />
  }
);

export default RootNavigator;
