export const loginConstrains = {
  email: {
    presence: true,
    email: true,
    length: {
      minimum: 3,
      message: 'must be at least 3 characters.'
    }
  },
  password: {
    presence: true,
    length: {
      minimum: 3,
      message: 'must be at least 3 characters'
    }
  }
};

export const registerConstrains = {
  email: {
    presence: true,
    email: true,
    length: {
      minimum: 3,
      message: 'must be at least 3 characters.'
    }
  },
  password: {
    presence: true,
    length: {
      minimum: 3,
      message: 'must be at least 3 characters'
    }
  },
  description: {
    presence: true,
    length: {
      minimum: 5,
      message: 'must be at least 5 characters'
    }
  },
  mobile: {
    presence: true,
    numericality: {
      onlyInteger: true
    },
    length: {
      minimum: 6,
      message: 'must be at least 6 characters'
    }
  }
};
